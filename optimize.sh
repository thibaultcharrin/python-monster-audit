#!/bin/bash

PERFS=(perflint)

for PERF in "${PERFS[@]}"; do
    python -m "$PERF" ./*.py
done

for PERF in "${PERFS[@]}"; do
    python -m "$PERF" tests/*.py
done

exit 0
