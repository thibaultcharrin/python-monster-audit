#!/bin/bash

ENVS=(pip-audit pipdeptree)

for ENV in "${ENVS[@]}"; do
    $ENV
done

exit 0
