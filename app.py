"""Main Flask API exposing several entrypoints."""
import json
import logging
import os

from flask import Flask, Response, request
from typeguard import typechecked

API_NAME: str = "api-for-openshift"
API_VERSION: str = str(os.getenv("API_VERSION", "unknown"))

app = Flask(__name__)


@app.post("/test")
@typechecked
def post_data() -> Response:
    """Perform test for data handling."""
    try:
        data = check_client()
    except ValueError as err:
        msg = err.args[0]
        status = err.args[1]
        response = construct_response(status="ko", msg=msg, data={})
        return Response(
            json.dumps(response), status=status, mimetype="application/json"
        )

    response = construct_response(status="ok", msg="", data=data)
    return Response(json.dumps(response), status=200, mimetype="application/json")


@typechecked
def check_client() -> dict | None:
    """Perform checks and return data."""
    payload = request.json
    data = None

    if (
        payload
        and isinstance(payload, dict)
        and payload.get("data")
        and isinstance(payload.get("data"), dict)
    ):
        data = payload.get("data")
    else:
        raise ValueError("Bad Request: the key 'data' was expected", 400)

    app.logger.debug("data=%s", data)
    return data


@typechecked
def construct_response(status: str, msg: str, data: dict | None) -> dict:
    """Enforce kwargs for a comprehensive data model."""
    response = {"status": status, "msg": msg, "data": data}

    app.logger.debug("response=%s", response)
    return response


@app.get("/health")
@typechecked
def get_info() -> Response:
    """Give information about the API."""
    info: dict = {"API": API_NAME, "Version": API_VERSION, "Team": "awesome-team"}

    app.logger.debug("info=%s", info)
    return Response(json.dumps(info), status=200, mimetype="Application/JSON")


if __name__ == "__main__":
    app.run()
else:
    gunicorn_logger = logging.getLogger("gunicorn.error")
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)
