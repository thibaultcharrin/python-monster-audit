#!/bin/bash

coverage run -m pytest -vv
coverage report -m --precision=2

exit 0
