#!/bin/bash

FORMATTERS=(mccabe 'radon cc' 'radon raw' 'radon mi' 'radon hal' black isort)

for FORMATTER in "${FORMATTERS[@]}"; do
    python -m $FORMATTER ./*.py
done

for FORMATTER in "${FORMATTERS[@]}"; do
    python -m $FORMATTER tests/*.py
done

exit 0
