"""UnitTest for the function get_info()."""
import unittest

import app

@unittest.skip("Needs fixing")
class GetInfoTest(unittest.TestCase):
    """Encapsulation of tests for the function get_info()."""

    def test_get_info(self):
        """Perform test to get API info."""

        info = app.get_info()
        self.assertEqual(info.json(), {"API": "api-for-openshift", "Version": "unknown", "Team": "awesome-team"})
