"""UnitTest for the function construct_response()."""
import unittest

import app


class ConstructResponseTest(unittest.TestCase):
    """Encapsulation of tests for the function construct_response()."""

    def test_construct_response(self):
        """Perform test to construct API response."""
        response = app.construct_response(
            status="ok", msg="", data={"data": {"key": "value"}}
        )
        self.assertEqual(
            response, {"status": "ok", "msg": "", "data": {"data": {"key": "value"}}}
        )
