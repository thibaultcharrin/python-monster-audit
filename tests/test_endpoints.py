"""UnitTest for the function post_data()."""
import unittest

from app import app


class PostDataTest(unittest.TestCase):
    """Encapsulation of tests for the function post_data()."""

    def setUp(self):
        self.app = app.test_client()

    @unittest.skip("Needs fixing")
    def test_get_info(self):
        """Perform test to get API info."""
        response = self.app.get("/health")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {"API": "awesome-api", "Team": "awesome-team"})

    def test_post_data_is_200(self):
        """Perform test to get API response 200."""
        response = self.app.post("/test", json={"data": {"key": "value"}})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json, {"status": "ok", "msg": "", "data": {"key": "value"}}
        )

    def test_post_oops_is_400(self):
        """Perform test to get API response 400."""
        response = self.app.post("/test", json={"oops": {"key": "value"}})
        print(f"{response.json=}")
        print(f"{response.data=}")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json,
            {
                "status": "ko",
                "msg": "Bad Request: the key 'data' was expected",
                "data": {},
            },
        )
