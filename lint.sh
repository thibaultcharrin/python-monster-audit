#!/bin/bash

LINTERS=(pylint pyflakes pycodestyle pydocstyle bandit mypy)

for LINTER in "${LINTERS[@]}"; do
    python -m "$LINTER" ./*.py
done

for LINTER in "${LINTERS[@]}"; do
    python -m "$LINTER" tests/*.py
done

exit 0
