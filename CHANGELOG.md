## 0.0.1 (2024-03-08)

### performance (1 change)

- [Use python:3.12-slim base image](thibaultcharrin/python-monster-audit@496dd8cd1f0045fa87624168250b5718833e6297) ([merge request](thibaultcharrin/python-monster-audit!2))

### changed (1 change)

- [Refactor gitlab-ci for component versions maintainability](thibaultcharrin/python-monster-audit@af9b9b2191031ef75669ee03e8a259ab584590c0) ([merge request](thibaultcharrin/python-monster-audit!1))
