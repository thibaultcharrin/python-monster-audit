#!/bin/bash

if [[ -z $VIRTUAL_ENV ]]; then
    echo "Please launch virtual env."
    echo "For example:"
    echo "  python3 -m venv ~/venv"
    echo "  source ~/venv/bin/activate"
    exit 1
fi

ENVS=(pip-audit pipdeptree)
FORMATTERS=(mccabe radon black isort)
LINTERS=(pylint pyflakes pycodestyle pydocstyle bandit mypy)
PERFS=(perflint snakeviz py-spy)
TESTS=(pytest coverage)

pip install --upgrade pip setuptools
pip install --upgrade "${ENVS[@]}" "${FORMATTERS[@]}" "${LINTERS[@]}" "${PERFS[@]}" "${TESTS[@]}"
pip install --upgrade -r requirements.txt

exit 0
