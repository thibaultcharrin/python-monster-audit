FROM python:3.12-slim

WORKDIR /usr/src/app
COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

COPY *.py ./
CMD ["gunicorn", "-w", "4", "-b", "0.0.0.0:8080", "--access-logfile=-", "--log-level=INFO", "app:app"]
